from keras.preprocessing.image import *
from CNN import CNN
from CNN import get_latest_model
from CNN import INPUT_DIR
from CNN import INPUT_SHAPE

model = CNN()
model.load_weights(get_latest_model())


def predict(file):
    '''
    predict whether file is a notes image
    '''
    img = load_img(file, target_size=INPUT_SHAPE)
    # x = .
    return model.predict_classes(np.expand_dims(img_to_array(img) / 255, axis=0))
    # return y
    # return np.squeeze(y) > 0.5


files = INPUT_DIR.listdir(pattern='*.png')

for count, file_path in enumerate(files):
    print(file_path, predict(file_path))
    # if not count % 10:
    #     print(str(count) + ' files examined')
    # if predict(file_path):  # check if the file is one of the notes
    #     file_name = file_path.split('/')[-1]  # get file name
    #     os.rename(file_path, notes_path + file_name)  # move the file to 'notes' folder
