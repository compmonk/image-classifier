'''This script goes along the blog post
"Building powerful image classification models using very little data"
from blog.keras.io.
It uses data that can be downloaded at:
https://www.kaggle.com/c/dogs-vs-cats/data
In our setup, we:
- created a data/ folder
- created train/ and validation/ subfolders inside data/
- created cats/ and dogs/ subfolders inside train/ and validation/
- put the cat pictures index 0-999 in data/train/cats
- put the cat pictures index 1000-1400 in data/validation/cats
- put the dogs pictures index 12500-13499 in data/train/dogs
- put the dog pictures index 13500-13900 in data/validation/dogs
So that we have 1000 training examples for each class, and 400 validation examples for each class.
In summary, this is our directory structure:
```
data/
    train/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
    validation/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
```
'''
import os
from datetime import datetime

from keras.preprocessing.image import ImageDataGenerator
from unipath import Path

from CNN import CNN
from CNN import IMAGE_HEIGHT
from CNN import IMAGE_WIDTH
from CNN import TRAIN_DIR
from CNN import TEST_DIR
from CNN import WEIGHTS_DIR

nb_train_samples = sum(
    [len(os.listdir(os.path.join(TRAIN_DIR, class_directory))) for class_directory in os.listdir(TRAIN_DIR)])
nb_validation_samples = sum(
    [len(os.listdir(os.path.join(TEST_DIR, class_directory))) for class_directory in os.listdir(TEST_DIR)])
epochs = 50
batch_size = 16

model = CNN()

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    TRAIN_DIR,
    target_size=(IMAGE_WIDTH, IMAGE_HEIGHT),
    batch_size=batch_size,
    class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
    TEST_DIR,
    target_size=(IMAGE_WIDTH, IMAGE_HEIGHT),
    batch_size=batch_size,
    class_mode='binary')

model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size)

model_path = Path(WEIGHTS_DIR, '{0}.h5'.format(str(datetime.now().strftime('%Y%m%d%H%M%S'))))
model.save_weights(model_path)

if model_path.exists():
    print("Model saved to {}".format(model_path))
else:
    print("Not able to save model")
