# Image Classifier

Classify images by providing training and testing data upload.


## Running it for the first time
```
git clone https://gitlab.com/compmonk/image-classifier.git
cd image-classifier
virtualenv venv
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
pip install -r requirements.txt
python extract.py
```



## To run
```
source venv/bin/activate
python extract.py
```


